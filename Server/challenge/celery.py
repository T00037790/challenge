import os
from celery import Celery
from kombu import Exchange, Queue

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'challenge.settings')

app = Celery('challenge')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
challenge_queue_name = 'challenge'
challenge_routing_key = 'challenge'
default_exchange_name = 'default'
default_exchange = Exchange(default_exchange_name, type='direct')

challenge_queue = Queue(
    challenge_queue_name,
    default_exchange,
    routing_key=challenge_routing_key)

app.conf.task_queues = (challenge_queue,)


