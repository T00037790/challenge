from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from .views import index, shoot

app_name = 'shooter'

urlpatterns = [
    path('', index, name="index"),
    path('shoot', shoot, name="shoot")
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
