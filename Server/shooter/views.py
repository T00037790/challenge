from django.shortcuts import render
from .tasks import challenge_task


def index(request):
    return render(request, 'shooter/index.html')


def shoot(request):
    challenge_task.apply_async(("ALVARO",), queue='challenge')
    return render(request, 'shooter/success.html')

