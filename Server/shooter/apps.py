from django.apps import AppConfig


class ShooterConfig(AppConfig):
    name = 'Server.shooter'
