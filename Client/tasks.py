from __future__ import absolute_import
from celery import Celery
import autoit
from time import sleep

app = Celery('tasks', broker='amqp://guest:guest@10.0.2.2//')


@app.task(name="shooter.tasks.challenge_task")
def challenge(text):
    autoit.run("notepad.exe file.txt")
    autoit.win_wait_active("[CLASS:Notepad]", 3)
    sleep(3)
    autoit.control_send("[CLASS:Notepad]", "Edit1", "^h")
    sleep(3)
    autoit.control_set_text("[CLASS:#32770]", "Edit1", "")
    sleep(3)
    autoit.control_send("[CLASS:#32770]", "Edit1", "TEXT")
    sleep(3)
    autoit.control_set_text("[CLASS:#32770]", "Edit2", "")
    sleep(3)
    autoit.control_send("[CLASS:#32770]", "Edit2", text)
    sleep(3)
    autoit.control_click("[Class:#32770]", "Button6")
    sleep(3)
    autoit.win_close("[CLASS:#32770]")
    sleep(3)
    autoit.control_send("[CLASS:Notepad]", "Edit1", "^a")
    sleep(3)
    autoit.control_send("[CLASS:Notepad]", "Edit1", "^c")
    sleep(3)
    autoit.control_send("[CLASS:Notepad]", "Edit1", "^n")
    sleep(3)
    autoit.control_click("[Class:#32770]", "Button2")
    sleep(2)
    autoit.control_send("[CLASS:Notepad]", "Edit1", "^v")
    sleep(3)
    autoit.control_send("[CLASS:Notepad]", "Edit1", "^s")
    sleep(3)
    autoit.control_send("[CLASS:#32770]", "Edit1", "file2")
    sleep(3)
    autoit.control_click("[Class:#32770]", "Button2")
    sleep(3)
    autoit.win_close("[CLASS:Notepad]")
    sleep(3)
    return "Success!"
